# addition_TP



## Installation

```
cd addition_tp/terraform
terraform init  && terraform plan
terraform apply
kubectl port-forward svc/wordpress 8085:80
```

## choix techniques


### Wordpress
pour le service Wordpress, j'ai mis en place les ressources suivants:

    * Deployment ( pour les ressouces, j'ai limité les pods en 512M et 256m CPU. j'ai choisi de favoriser le HPA pour la monter en charge qui vari entre 1 à 5 pods )
    * service 
    * ServiceAccount
    * configMap
    * hpa 
    

### Mariadb
Pour le service Mariadb, j'ai mis en place les services suivants:

    * StatefulSet
    * PVC
    * service
    * Secret
