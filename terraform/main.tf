resource "helm_release" "wordpress" {
  name       = "wordpress"
  repository = ""
  chart      = "../wordpress"
  namespace  = "default"
  version    = "0.1.0"
  values = [
  ]
}
